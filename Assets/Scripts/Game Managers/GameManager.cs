using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] int numberOfClicks;

    CubeBasedUI UIController;
    public CubeBasedBoard BoardController;

    public PlayerController player;

    public bool gameFinished;


    private void Awake()
    {
        instance = this;
    }

    public delegate void PauseAction(bool isPaused);
    public event PauseAction OnPaused;

    public Action<bool> GamePaused;
    // Start is called before the first frame update
    void Start()
    {
        UIController = FindObjectOfType<CubeBasedUI>();
        BoardController = FindObjectOfType<CubeBasedBoard>();
        player = FindObjectOfType<PlayerController>();
       
    }

    public void CallGamePaused(bool isPaused)
    {
        GamePaused(isPaused);
        Time.timeScale = isPaused ? 0f : 1f;
    }


   public void IncrementClicks()
    {
        if (UIController)
        {
            numberOfClicks++;
            UIController.IncreaseClicks(numberOfClicks);
        }
    }
    public void ResetClicks()
    {
        if (UIController)
        {
            numberOfClicks = 0;
            UIController.IncreaseClicks(numberOfClicks);
        }
    }
}
