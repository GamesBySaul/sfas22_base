using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;
public class GolfModeController : MonoBehaviour
{

    public static GolfModeController instance;
    private void Awake()
    {
        instance = this;
    }

    public GameObject golfStartObj;
    public GameObject golfHoleObj;
    public GameObject golfBallPrefab;
    [SerializeField] GameObject golfUI;
    [SerializeField] Transform golfParent;
    public Transform ballSpawn;

    GameObject golfBall;
    GameObject golfStart;
    GameObject golfHole;

    public bool golfGenerated = false;


    public Slider powerBar;
    public TextMeshProUGUI shotText;

    int holePlacementsTried = 0;
    bool placeAnyways;

    public bool shotMade;
    public int shots;

    public Vector3 golfStartPos, golfHolePos;

    public int minesHit;
    private void Start()
    {
        powerBar.gameObject.SetActive(false);

    }
    public void GenerateGolfStart(Vector3 firstRow, Vector3 lastRow)
    {
        
        float startPosX = firstRow.x - 1;

        float startPosY = Random.Range(firstRow.y, lastRow.y);
        startPosY = (Mathf.Round(startPosY) * 2f) / 2f;

        int checker = Random.Range(0, 2);
        float adder = checker != 1 ? -.5f : .5f;

        startPosY += adder;


        startPosY = Mathf.Clamp(startPosY, lastRow.y, firstRow.y);
        golfStartPos = new Vector3(startPosX, startPosY, 1f);
        //if (CheckNeighbour(firstRow, startPosY))
        //{
        //    Debug.Log("Was Dangerous - Reshuffling");
        //    GameManager.instance.BoardController.RechargeBoxes();
        //    //GenerateGolfHole(firstRow, lastRow);
        //    return;
        //}


        GenerateGolfHole(firstRow, lastRow);
        shots = 0;
        UpdateShotText();
    }

    public void GenerateGolfHole(Vector3 firstRow, Vector3 lastRow)
    {
        //if(holePlacementsTried > GameManager.instance.BoardController.Width)
        //{
        //    Debug.Log("Impossible to place Hole not next to mine");
        //    placeAnyways = true;
        //}
        float startPosX = lastRow.x + 1;

        float startPosY = Random.Range(firstRow.y, lastRow.y);
        startPosY = (Mathf.Round(startPosY) * 2f ) / 2f;

        int checker = Random.Range(0, 2);
        float adder = checker != 1 ? -.5f : .5f;
       
        startPosY += adder;

        
        startPosY = Mathf.Clamp(startPosY, lastRow.y, firstRow.y);
        golfHolePos = new Vector3(startPosX, startPosY, 1f);


        //if (CheckNeighbour(lastRow, startPosY) && !placeAnyways)
        //{
        //    //Debug.Log("Was Dangerous - Reshuffling");
        //    holePlacementsTried++;
        //    GenerateGolfHole(firstRow, lastRow);
        //    return;
        //}
        holePlacementsTried = 0;
        placeAnyways = false;

        GameManager.instance.BoardController.CheckGolfObjectNeighbours(golfStartPos, golfHolePos);

    }
    public bool CheckNeighbour(Vector3 rowToCheck, float yVal)
    {
        bool nearMine = false;
        Vector2 posToCheck = new Vector2(rowToCheck.x, yVal);
        CubeBasedBoard cubeBoard = GameObject.FindGameObjectWithTag("GolfBoard").GetComponent<CubeBasedBoard>();
        if (cubeBoard.cubeDict.ContainsValue(posToCheck))
        {
            CubeInfo cube = cubeBoard.cubeDict.Single(vector => vector.Value == posToCheck).Key;
            //Debug.Log(cube.gameObject.name);
            if (cube.IsDangerous)
            {
                //Debug.Log("Is a dangerous cube");
                nearMine = true;
            }
            else
            {
                nearMine = false;
            }
        }

        return nearMine;
    }

    public void SpawnStartAndHole()
    {
        golfStart = Instantiate(golfStartObj, golfParent);

        golfStart.transform.position = golfStartPos;

        golfHole = Instantiate(golfHoleObj, golfParent);
        golfHole.transform.position = golfHolePos;

        ballSpawn = golfStart.transform.GetChild(0);
        GenerateGolfBall();
    }
    public void GenerateGolfBall()
    {
        golfBall = Instantiate(golfBallPrefab, golfParent);
        golfBall.transform.position = ballSpawn.transform.position;
        
        golfGenerated = true;
    }
    public void SwapToGolfMode()
    {
        if(CameraManager.instance.startInterp)
        {
            //CameraManager.instance.golfPos = CameraManager.instance.panObject.transform.position + CameraManager.instance.ballScr.offset;
            return;
        }
        CameraManager.instance.golfInterp = true;
        EventSystem.current.SetSelectedGameObject(null);
      
    }

    public void ToggleUI()
    {
        golfUI.SetActive(!golfUI.activeSelf);
    }

    public void DeleteGolfObjects()
    {
        
        if(golfStart)
        {
            Destroy(golfStart);
        }
        if(golfBall)
        {
            Destroy(golfBall);
        }
        if(golfHole)
        {
            Destroy(golfHole);
        }
        golfGenerated = false;
        shotMade = false;
        shots = 0;
    }

    public void UpdateShotText()
    {
        if (shotText != null)
        {
            shotText.text = ($"Shots: {shots}");
        }
    }

    public Vector3 GetBallTransform()
    {
        return golfBall.transform.position;
    }

    public Vector3 GetHoleTransform()
    {
        return golfHole.transform.position;
    }
}
