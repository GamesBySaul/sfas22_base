using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallStartController : MonoBehaviour
{
    public Transform ballSpawn;
    // Start is called before the first frame update
    void Start()
    {
        ballSpawn = GetComponentInChildren<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
