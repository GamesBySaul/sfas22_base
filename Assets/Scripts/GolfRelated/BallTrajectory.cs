using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LineRenderer))]
public class BallTrajectory : MonoBehaviour
{
    public LineRenderer lineRend;

    [SerializeField] float minPower = 1f;
    [SerializeField] float maxPower = 2f;

   public Slider powerBar;
    private void Awake()
    {
        lineRend = GetComponent<LineRenderer>();

    }

    private void OnEnable()
    {
        powerBar = GolfModeController.instance.powerBar;
    }

    public void RenderLine(Vector3 startPos, Vector3 endPos)
    {
       

        lineRend.positionCount = 2;
        Vector3[] points = new Vector3[lineRend.positionCount];
        points[0] = startPos;
        points[1] = endPos;

        lineRend.SetPositions(points);
    }

    public void EndLine()
    {
       
        lineRend.positionCount = 0;
    }

    public float PullBackPower()
    {
        //HOW DO
        float difference = Vector3.Distance(lineRend.GetPosition(0), lineRend.GetPosition(1)) - 4f;
        //Debug.Log(difference);


        float pullBack = Mathf.Abs(difference);
        powerBar.value = Mathf.Clamp(pullBack, powerBar.minValue, powerBar.maxValue);
        //Debug.Log(pullBack);
        pullBack = powerBar.value * 10f;
        //pullBack = Mathf.Clamp(pullBack, minPower, maxPower);
        //float pullBack = Vector3.Distance(lineRend.GetPosition(0).normalized, lineRend.GetPosition(1).normalized);
        //Debug.Log(pullBack);

        return powerBar.value * 10f;
    }
}
