using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolfBallController : MonoBehaviour
{
    Rigidbody rb;


    public Vector3 offset;
    public GameObject ballHitSpot;
    public GameObject camObject;
    Vector3 mouseOriginPos;

    BallTrajectory trajectoryScr;

    public float depth;
    public float force;

    [SerializeField] float pullBackPower;

    bool addForce = false;
    bool addDangerForce = false;

    [SerializeField] bool followBall;

    bool checkVelocity;

    bool canShoot = true;


    Vector3 blastVelocity;

    Vector3 lastPosition;


    [SerializeField] float fallDistance;

    AudioSource audSrc;
    [SerializeField] AudioClip fallOff, golfShot;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        audSrc = GetComponent<AudioSource>();
        camObject = GameObject.FindGameObjectWithTag("GolfCam");
        
        CameraManager.instance.ballParent = camObject;
        CameraManager.instance.panObject = ballHitSpot;
        CameraManager.instance.golfPos = ballHitSpot.transform.position + offset;
        trajectoryScr = GetComponent<BallTrajectory>();
        CameraManager.instance.canRotateAround = true;
        //Camera.main.transform.SetParent(camObject.transform);
        CameraManager.instance.ballScr = this;
      
        canShoot = true;
        followBall = false;
        lastPosition = transform.position;
    }

    private void FixedUpdate()
    {
        camObject.transform.position = ballHitSpot.transform.position;
        camObject.transform.rotation = ballHitSpot.transform.rotation;
        if (CameraManager.instance.isInGolfMode)
        {


            if (gameObject.transform.position.z >= fallDistance)
            {
                rb.velocity = Vector3.zero;

                gameObject.transform.position = lastPosition;
                GolfModeController.instance.shots++;
                if(!audSrc.isPlaying)
                {
                    audSrc.PlayOneShot(fallOff);
                }
            }
        }

        if (addForce)
        {
            CameraManager.instance.canRotateAround = false;
           
            followBall = true;
            addForce = false;
            Vector3 dir = Vector3.zero;
            if (trajectoryScr.lineRend.positionCount > 0)
            {
                dir = (trajectoryScr.lineRend.GetPosition(1) - mouseOriginPos).normalized;
            }
            dir = new Vector3(dir.x, dir.y, 0f);
            trajectoryScr.EndLine();

            rb.AddForce(-dir * force * pullBackPower);
            pullBackPower = 1.0f;

            StartCoroutine(ShotCheckCooldown());
        }
        if(addDangerForce)
        {
            CameraManager.instance.canRotateAround = false;

            followBall = true;
            addDangerForce = false;

            trajectoryScr.EndLine();
            pullBackPower = 2.0f;
            Vector3 forceToAdd = Vector3.ClampMagnitude(blastVelocity * force * pullBackPower, 1000f);
            rb.velocity = Vector3.zero;

            rb.AddForce(forceToAdd);

            StartCoroutine(ShotCheckCooldown());
        }

        if (checkVelocity && CameraManager.instance.isInGolfMode)
        {
            if (rb.velocity.magnitude <= 0f && followBall && !GolfModeController.instance.shotMade && !CameraManager.instance.golfInterp)
            {
                lastPosition = gameObject.transform.position;
                Camera.main.transform.SetParent(camObject.transform);
                CameraManager.instance.canRotateAround = true;
                //Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, camObject.transform.position + offset, Time.deltaTime * 10f);
                followBall = false;
                checkVelocity = false;
                canShoot = true;

            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        CubeInfo cubeHit = collision.gameObject.GetComponent<CubeInfo>();
        GolfHoleController hole = collision.gameObject.GetComponent<GolfHoleController>();
        if(!cubeHit && !hole)
        {
            return;
        }

        if(cubeHit)
        {
            cubeHit.PlayAudio();
        }

        if(cubeHit && (cubeHit.IsDangerous && cubeHit.isActiveAndEnabled))
        {
            blastVelocity = rb.velocity * -2f;
            addDangerForce = true;
            cubeHit.golfClicked = true;
            cubeHit.MoveCube();
            GolfModeController.instance.shots++;
            GolfModeController.instance.minesHit++;
        }

        if(hole)
        {
            hole.BallReceived();
        }

    }
    private void OnMouseDrag()
    {
       
        if (!CameraManager.instance.isInGolfMode || !canShoot)
        {
            return;
        }
        mouseOriginPos = ballHitSpot.transform.position;
       
        Vector3 currentPos = Camera.main.ScreenPointToRay(Input.mousePosition).direction + Camera.main.ScreenPointToRay(Input.mousePosition).origin * depth;

        //Vector3 currentPos = Camera.main.ScreenToWorldPoint(Input.mousePosition) * depth;

        trajectoryScr.RenderLine(mouseOriginPos, currentPos);
        pullBackPower = trajectoryScr.PullBackPower();

        trajectoryScr.powerBar.gameObject.SetActive(true);

    }

    private void OnMouseUp()
    {
        if (!CameraManager.instance.isInGolfMode || !canShoot)
        {
            return;
        }
        //Camera.main.transform.parent = null;
       
        //Camera.main.transform.position = ballHitSpot.transform.position + offset;
        //Camera.main.transform.LookAt(ballHitSpot.transform, Vector3.right);
        
        //fixCamTransform = true;
        
        //CameraManager.instance.fixCamRotation = true;
        addForce = true;
        trajectoryScr.powerBar.gameObject.SetActive(false);
        canShoot = false;
        GolfModeController.instance.shots++;
        GolfModeController.instance.UpdateShotText();
        audSrc.PlayOneShot(golfShot);
    }
    public IEnumerator ShotCheckCooldown()
    {
        yield return new WaitForSeconds(1.5f);
        checkVelocity = true;
    }
}
