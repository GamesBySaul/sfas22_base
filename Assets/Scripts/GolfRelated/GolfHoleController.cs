using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolfHoleController : MonoBehaviour
{

    public void BallReceived()
    {
      
        GolfModeController.instance.shotMade = true;
        GolfModeController.instance.SwapToGolfMode();
        GameManager.instance.BoardController.GolfShotMade();
    }
}
