using System.Collections.Generic;

public static class ListHelper
{
    public static int seedToUse;

    public static void RandomShuffle<T>(this IList<T> list)
    {
        UnityEngine.Random.InitState(seedToUse);
        var count = list.Count;
        var last = count - 1;
        for (var index = 0; index < last; ++index)
        {
            var r = UnityEngine.Random.Range(index, count);
            var swap = list[index];
            list[index] = list[r];
            list[r] = swap;
        }
    }
}