using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class MainMenuController : MonoBehaviour
{
    public static MainMenuController instance;

    private void Awake()
    {
      
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }


    AsyncOperation loadingOperation;

  
   

    bool startLoading;
    bool moveBars;
    bool reverseBars;
    [SerializeField] GameObject menuObject;
    [SerializeField] GameObject panelObject;
    [SerializeField] GameObject loadingObject;

    [SerializeField] RectTransform loading1, loading2;
    float height1, height2;

    float timer;
    [SerializeField] float timeEasing;

    float desHeight;

    int buildIndex;

    [SerializeField] Slider sizeSlider, mineSlider;
    public int gridSizeValue, mineAmount;
    [SerializeField] TextMeshProUGUI sizeText, mineText;
    [SerializeField] Button gameButton;
    [SerializeField] Button copyButton;
    [SerializeField] TMP_InputField seedInput;

    [SerializeField] GameObject titleText;

    [SerializeField] GameObject socialCanvas;

    public int currentSeed;

    private void Start()
    {
        height1 = loading1.rect.height;
        height2 = loading2.rect.height;
    }

    public void GoToGame(int bIndex)
    {
        sizeSlider.gameObject.SetActive(false);
        mineSlider.gameObject.SetActive(false);

        socialCanvas.SetActive(false);

        panelObject.SetActive(false);

        mineAmount = NumberOfMines();

        gridSizeValue = (int)sizeSlider.value;
        buildIndex = bIndex;
        menuObject.SetActive(false);
        loadingObject.SetActive(true);
        seedInput.gameObject.SetActive(false);
        desHeight = 540f;
        moveBars = true;
        titleText.SetActive(false);
        //Debug.Log(seedInput.text);
        if(string.IsNullOrEmpty(seedInput.text))
        {
            ListHelper.seedToUse = Random.Range(10000000, 100000000);
            
        }
        else
        {
            ListHelper.seedToUse = int.Parse(seedInput.text);
        }
        currentSeed = ListHelper.seedToUse;
        seedInput.text = currentSeed.ToString();
        
    }

    public void CopySeedText()
    {
        if(!string.IsNullOrEmpty(seedInput.text))
        {
            GUIUtility.systemCopyBuffer = seedInput.text;
        }
    }


    public void SetSeedText(int seed)
    {
        seedInput.text = seed.ToString();
    }

    int NumberOfMines()
    {
        return Mathf.CeilToInt(mineSlider.value * Mathf.Pow(sizeSlider.value, 2));
    }

    private void Update()
    {
        if(moveBars)
        {
            timer += Time.deltaTime * timeEasing;
   
            HandleBarMovement(desHeight);
        }
        if (SceneManager.GetActiveScene().buildIndex == 0) 
        {
            gameButton.interactable = seedInput.text.Length >= 8 || seedInput.text.Length <= 0;
            copyButton.interactable = seedInput.text.Length >= 8;
        }

        if(sizeSlider.gameObject.activeInHierarchy)
        {
            sizeText.text = $"Game Board Size: {sizeSlider.value}";
            mineText.text = $"Number Of Mines: {NumberOfMines()}";
        }


        if (startLoading)
        {
           
           
            
            if(loadingOperation.isDone)
            {
                startLoading = false;
                
                desHeight = 0f;
                moveBars = true;

                
            }
        }
        
    }

    public void HandleBarMovement(float desiredHeight)
    {
        height1 = Mathf.Lerp(loading1.rect.height, desiredHeight, timer * timeEasing);
        height2 = Mathf.Lerp(loading2.rect.height, desiredHeight, timer * timeEasing);
        loading1.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height1);
        loading2.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height2);
        if (timer > .15f)
        {
            if (desiredHeight <= 0f)
            {
                ReEnableUI();
            }
        }
        if (timer > .45f)
        {
            moveBars = false;
            timer = 0;
            if (desiredHeight <= 0f)
            {
                loadingObject.SetActive(false);
               
                return;
            }
            else
            {
                loadingOperation = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + buildIndex);
                startLoading = true;
                
               
            }
        }

    }

    void ReEnableUI()
    {
        if (SceneManager.GetActiveScene().buildIndex < 1)
        {
            menuObject.SetActive(true);
            sizeSlider.gameObject.SetActive(true);
            mineSlider.gameObject.SetActive(true);
            seedInput.gameObject.SetActive(true);
            titleText.SetActive(true);
            panelObject.SetActive(true);
            socialCanvas.SetActive(true);

        }
    }

    public void QuitGame()
    {
#if !UNITY_EDITOR
        Application.Quit();
#else
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }

}
