using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class PauseMenuController : MonoBehaviour
{

    

    public bool bIsPaused;

    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject pauseButton;
    [SerializeField] TextMeshProUGUI seedText;

    [SerializeField] GameObject mainUI;
    [SerializeField] GameObject golfUI;

    [SerializeField] Toggle wallToggle;
    bool changeWalls;
    // Start is called before the first frame update
    void Start()
    {
        seedText.text = $"Current Seed: {ListHelper.seedToUse}";
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && !GameManager.instance.gameFinished)
        {
            TogglePause();

        }
    }



    public void TogglePause()
    {
        bIsPaused = !bIsPaused;
        GameManager.instance.CallGamePaused(bIsPaused);
        pauseMenu.SetActive(bIsPaused);
        EventSystem.current.SetSelectedGameObject(null);

        if(changeWalls)
        {
            changeWalls = false;
            GameManager.instance.BoardController.ToggleWalls(wallToggle.isOn);
        }
    }

    public void ToggleWalls()
    {
        changeWalls = true;
    }

    public void Retry()
    {
        TogglePause();

        GolfModeController.instance.DeleteGolfObjects();
        mainUI.SetActive(false);
        golfUI.SetActive(false);
        pauseButton.SetActive(false);
        MainMenuController.instance.SetSeedText(MainMenuController.instance.currentSeed);

        MainMenuController.instance.GoToGame(0);
    }

    public void MainMenu()
    {
        TogglePause();
        
        mainUI.SetActive(false);
        golfUI.SetActive(false);
        pauseButton.SetActive(false);
        MainMenuController.instance.SetSeedText(MainMenuController.instance.currentSeed);

        MainMenuController.instance.GoToGame(-1);
    }
    

    public void CopyLevelSeed()
    {
        MainMenuController.instance.CopySeedText();
    }

}
