using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BoardGenerator : MonoBehaviour
{
    public CubeBasedBoard boardController;
   
    public void DebugGenerate()
    {
        if (boardController)
        {
            boardController.BoardGeneration();
        }
    }

    public void ShowBoard()
    {
        if (boardController)
        {
            boardController.ShowBoardDebug();
        }
    }

    public void DestroyBoard()
    {
        if(boardController)
        {
            boardController.BoardDeGeneration();
        }
    }
}
