using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class CubeBasedBoard : MonoBehaviour
{
    public enum Event { ClickedOther, ClickedDanger, Win };

  
    public int Width = 10;
    [SerializeField] private int Height = 10;
    [SerializeField] private int NumberOfDangerousBoxes = 10;

    public Vector2Int[] cubeNeighbours;
    private Action<Event> _clickEvent;

    public Dictionary<CubeInfo, Vector2> cubeDict = new Dictionary<CubeInfo, Vector2>();

    public GameObject cubePrefab;
    public GameObject wallPrefab;
    private List<GameObject> walls = new List<GameObject>();
    public List<CubeInfo> cubeGrid;

    [SerializeField] private Material cubeMat;

    public bool boardIsGenerated = false;
    GameObject parentObj;

    bool recheckGolf;


    public void Setup(Action<Event> onClickEvent)
    {
       
        _clickEvent = onClickEvent;
        Clear();
    }

    public void Clear()
    {
        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                int index = row * Width + column;
                cubeGrid[index].StandDown();
            }
        }
    }

    public void RechargeBoxes()
    {
        int numberOfItems = Width * Height;
        List<bool> dangerList = new List<bool>(numberOfItems);

        for (int count = 0; count < numberOfItems; ++count)
        {
            dangerList.Add(count < NumberOfDangerousBoxes);
        }

        dangerList.RandomShuffle();

        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                int index = row * Width + column;
               
                cubeGrid[index].Charge(CountDangerNearby(dangerList, index), dangerList[index], OnClickedBox);

                cubeGrid[index].GetComponent<MeshRenderer>().material = cubeMat;
                cubeGrid[index].defMat = cubeMat;
            }
        }
        if(!recheckGolf)
        {
            if (!GolfModeController.instance.golfGenerated)
            {
                GolfModeController.instance.GenerateGolfStart(cubeGrid[0].transform.position, cubeGrid[cubeGrid.Count - 1].transform.position);

            }

            return;
        }
        if(recheckGolf)
        {
            ListHelper.seedToUse = UnityEngine.Random.Range(10000000, 100000000);
            CheckGolfObjectNeighbours(GolfModeController.instance.golfStartPos, GolfModeController.instance.golfHolePos);
        }
    }

    public void CheckGolfObjectNeighbours(Vector2 golfStart, Vector2 golfHole)
    {
        recheckGolf = true;
        Vector2 startPosToCheck = new Vector2(cubeGrid[0].transform.position.x, golfStart.y);
        Vector2 holePosToCheck = new Vector2(cubeGrid[cubeGrid.Count - 1].transform.position.x, golfHole.y);
        if (cubeDict.ContainsValue(startPosToCheck) && cubeDict.ContainsValue(holePosToCheck))
        {
            CubeInfo cube = cubeDict.Single(vector => vector.Value == startPosToCheck).Key;
            if (cube.IsDangerous)
            {
                RechargeBoxes();
                return;
            }
            if(NumberOfDangerousBoxes == ((Height * Width) -1))
            {
                GolfModeController.instance.SpawnStartAndHole();
                return;
            }
            cube = cubeDict.Single(vector => vector.Value == holePosToCheck).Key;
            if (cube.IsDangerous)
            {
                RechargeBoxes();
                return;
            }

            GolfModeController.instance.SpawnStartAndHole();
        }
        else
        {
            Debug.Log("Invalid positions");
        }
    }

    private void Awake()
    {

        //Debug.Log(MainMenuController.instance.gridSizeValue);

        Height = MainMenuController.instance.gridSizeValue;
        Width = Height;

        NumberOfDangerousBoxes = MainMenuController.instance.mineAmount;
        BoardGeneration();

        CheckNeighbourBoxes();
        CameraManager.instance.SetEndSize(Width);

    }

    public void BoardGeneration()
    {
        Vector2 offset = new Vector2(0, .5f);
        if (Width % 2 == 0 || Height % 2 == 0)
        {
            offset = new Vector2(.5f, .5f);
        }

        Vector2 startPos = new Vector2(Width / -2, Height / 2) + offset;
        parentObj = new GameObject("Board Parent");
        parentObj.transform.SetParent(this.transform);
        for (int row = 0; row < Width; ++row)
        {

            GameObject rowObj = new GameObject(string.Format("Row{0}", row));
            rowObj.transform.SetParent(parentObj.transform);
            rowObj.transform.position = new Vector2(startPos.x, startPos.y - (1 * row));


            for (int column = 0; column < Height; ++column)
            {
                int index = row * Width + column;

                cubeGrid.Add(Instantiate(cubePrefab.GetComponent<CubeInfo>(), rowObj.transform));
                cubeGrid[index].transform.position = new Vector2(startPos.x + (1 * (column)), startPos.y - (1 * row));
                cubeGrid[index].name = string.Format("ID{0}, Row{1}, Column{2}", index, row, column);
                cubeGrid[index].Setup(index, row, column);
                cubeGrid[index].GetComponent<MeshRenderer>().material = cubeMat;
                cubeGrid[index].defMat = cubeMat;
                cubeDict.Add(cubeGrid[index], cubeGrid[index].transform.position);

                cubeGrid[index].gameObject.SetActive(false);
            }
        }
        
        //GolfModeController.instance.GenerateGolfStart(cubeGrid[0].transform.position);
        //GolfModeController.instance.GenerateGolfHole(cubeGrid[0].transform.position,cubeGrid[cubeGrid.Count - 1].transform.position);
        boardIsGenerated = true;
    }

    public void GenerateWalls()
    {
        
        for (int numWalls = 0; numWalls < 6; ++numWalls)
        {
            walls.Add(Instantiate(wallPrefab, parentObj.transform));
            walls[numWalls].SetActive(false);
        }
        int wallX = 1;
        int wallY = 1;
        foreach (GameObject wall in walls)
        {
            int index = walls.IndexOf(wall);
            int multiplier = 0;
            if (index == 0 || index == 1) //X walls
            {
                multiplier = (wallX % 2 == 0 ? -1 : 1);
                wall.transform.localScale = new Vector3(Width, wall.transform.localScale.y, wall.transform.localScale.z);
                wall.transform.eulerAngles = new Vector3(90f, 90f, 90f);
                float yPos = (wallX % 2 == 0 ? -.25f : .75f);
                float yAddition = 0;
                if (yPos == -.25)
                {
                    yAddition = (Width % 2 == 0 ? -1 : 0);
                }
                wall.transform.position = new Vector3(0, (cubeGrid[0].transform.parent.position.y + yPos + yAddition) * multiplier, wall.transform.position.z);
                wall.gameObject.name = $"WallX: {wallX}";
                wallX++;

            }

            else
            {
                float yScale = 1;
                float yAddition = 0;
                float yPos = 0;
                if (index == 2)
                {
                     yScale = cubeGrid[0].transform.parent.position.y - GolfModeController.instance.GetBallTransform().y;
                    yAddition = (Width % 2 == 0 ? 1 : .5f);
                    yAddition += (yScale / 2) - (Width % 2 == 0 ? .5f : 0f);
                    yPos = GolfModeController.instance.GetBallTransform().y;
                }
                else if(index ==4)
                {
                     yScale = GolfModeController.instance.GetBallTransform().y- cubeGrid[cubeGrid.Count-1].transform.parent.position.y;
                    yAddition = (Width % 2 == 0 ? -1 : -.5f);
                    yAddition -= (yScale / 2) - (Width % 2 == 0 ? .5f : 0f);
                    yPos = GolfModeController.instance.GetBallTransform().y;
                } 
                else if(index ==5)
                {
                     yScale = cubeGrid[0].transform.parent.position.y - GolfModeController.instance.GetHoleTransform().y;
                    yAddition = (Width % 2 == 0 ? 1 : .5f);
                    yAddition += (yScale / 2) - (Width % 2 == 0 ? .5f : 0f);
                    yPos = GolfModeController.instance.GetHoleTransform().y;
                }
                else
                {
                     yScale = GolfModeController.instance.GetHoleTransform().y - cubeGrid[cubeGrid.Count - 1].transform.parent.position.y;
                    yAddition = (Width % 2 == 0 ? -1 : -.5f);
                    yAddition -= (yScale / 2) - (Width % 2 == 0 ? .5f : 0f);
                    yPos = GolfModeController.instance.GetHoleTransform().y;
                }
                wall.transform.localScale = new Vector3(wall.transform.localScale.x, yScale, wall.transform.localScale.z);

                multiplier = (wallY % 2 == 0 ? -1 : 1);
                wall.transform.eulerAngles = new Vector3(180, 90f, 180f);
                //float yPos = (Width % 2 == 0 ? 1 : .5f);
                //First Wall Y needs to start at -1 until the player makes a shot
                wall.transform.position = new Vector3((cubeGrid[(cubeGrid.Count - 1) / 2].transform.parent.position.x - .75f) * multiplier, yPos + yAddition, wall.transform.position.z);
                wall.gameObject.name = $"WallY: {wallY}";
                wallY++;
            }
            wall.SetActive(true);
        }
    }

    public void BoardDeGeneration()
    {
        if(parentObj == null)
        {
            return;
        }
        DestroyImmediate(parentObj);
        cubeGrid.Clear();
        cubeDict.Clear();
        walls.Clear();
        boardIsGenerated = false;
        recheckGolf = false;
    }

    public void ShowBoardDebug()
    {
        if(cubeGrid.Count > 0)
        {
            for(int i = 0; i <cubeGrid.Count; i++)
            {
                cubeGrid[i].gameObject.SetActive(true);
            }
        }
    }


    public void ToggleCubes(bool enabled)
    {
        foreach(CubeInfo Cube in cubeGrid)
        {
            Cube.gameObject.SetActive(enabled);
        }
        GenerateWalls();
    }

    private int CountDangerNearby(List<bool> danger, int index)
    {
       
        int result = 0;

        if (!danger[index]) 
        {

            for (int count = 0; count < cubeGrid[index].neighbourBoxes.Count; count++)
            {


                int neighbourIndex = cubeGrid.IndexOf(cubeGrid[index].neighbourBoxes[count]);
                if(neighbourIndex < danger.Count && danger[neighbourIndex])
                {
                    result++;
                }

            }
        }

        return result;
    }

    public void CheckNeighbourBoxes()
    {

        for (int currentCube = 0; currentCube < cubeGrid.Count; currentCube++)
        {
            for (int count = 0; count < cubeNeighbours.Length; count++)
            {
                Vector2 currentPos = cubeGrid[currentCube].transform.position;
                foreach (KeyValuePair<CubeInfo, Vector2> cube in cubeDict)
                {
                    if (cube.Value == currentPos + cubeNeighbours[count])
                    {
                        cubeGrid[currentCube].neighbourBoxes.Add(cube.Key);
                    }
                }

            }
        }

    }

    private void OnClickedBox(CubeInfo box)
    {
        Event clickEvent = Event.ClickedOther;

        if (box.IsDangerous)
        {
            clickEvent = Event.ClickedDanger;
        }
        else if(box.danNearby <= 0)
        {
            ClearNearbyBlanks(box);
        }
        else
        {
            box.MoveCube();
        }
        if (CheckForWin()) //this is where the game ends.
        {
            clickEvent = Event.Win;
        }

        _clickEvent?.Invoke(clickEvent);
    }
    public void GolfShotMade()
    {
        Event clickEvent = Event.ClickedOther;
        if(CheckForWin())
        {
            clickEvent = Event.Win;
        }
        _clickEvent?.Invoke(clickEvent);
    }
    private bool CheckForWin()
    {
        bool Result = true;

        for (int count = 0; Result && count < cubeGrid.Count; ++count)
        {
            if (!cubeGrid[count].IsDangerous && cubeGrid[count].IsActive)
            {
                Result = false;
            }
        }

        if(!GolfModeController.instance.shotMade)
        {
            Result = false;
        }

        return Result;
    }

    private void ClearNearbyBlanks(CubeInfo box)
    {
        RecursiveClearBlanks(box);
    }

    private void RecursiveClearBlanks(CubeInfo box)
    {
        if (!box.IsDangerous)
        {
            box.Reveal();
            box.SetDangerFlagDisabled();
            if (box.DangerNearby == 0)
            {
                int currentIndex = cubeGrid.IndexOf(box);
                for (int count = 0; count < box.neighbourBoxes.Count; count++)
                {
                    int neighbourIndex = cubeGrid.IndexOf(box.neighbourBoxes[count]);
                    if (cubeGrid[neighbourIndex].IsActive)
                    {
                        RecursiveClearBlanks(cubeGrid[neighbourIndex]);
                    }
                }

            }
        }
    }

    public void CheckIfBoxHit(Vector2 pos)
    {  
        foreach (KeyValuePair<CubeInfo, Vector2> keyval in cubeDict)
        {

            if (Mathf.Abs(Vector2.Distance(pos, keyval.Value)) <= .6f)
            {
                GameManager.instance.player.currentCube = keyval.Key;
                return;
            }
        }
    }


    public void ToggleWalls(bool turnOff)
    {
        //Scale them down or up depending on toggle
        foreach(GameObject wall in walls)
        {
            
            wall.SetActive(turnOff);

        }


    }

}
