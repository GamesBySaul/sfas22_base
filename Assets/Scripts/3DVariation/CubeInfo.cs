using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
public class CubeInfo : MonoBehaviour
{
    [SerializeField] private Color[] DangerColors = new Color[8];
    [SerializeField] private Image Danger;
    [SerializeField] private Image DangerFlag;

    private TMP_Text _textDisplay;
    [SerializeField] private Button _button;
    private Action<CubeInfo> _changeCallback;

    public List<CubeInfo> neighbourBoxes = new List<CubeInfo>();
    public List<CubeInfo> dangerBoxes = new List<CubeInfo>();

    MeshRenderer rend;
    public Material defMat;
    [SerializeField] Material highlightedMat;
    [SerializeField] Material clickedMat;
    [SerializeField] Material mineMat;

    public CubeMovementController movementScr;


    Vector3 initialPos;

    public int RowIndex { get; private set; }
    public int ColumnIndex { get; private set; }
    public int ID { get; private set; }
    public int DangerNearby { get; private set; }
    public bool IsDangerous { get; private set; }
    public bool IsActive { get { return _button != null && _button.interactable; } }

    public bool isDanger;
    public int danNearby;

    [SerializeField] PhysicMaterial bouncyMaterial;
    
    [SerializeField] BoxCollider collider;

    public bool golfClicked;

    AudioSource audSrc;
    [SerializeField] AudioClip explosion, blockHit;
    public void Setup(int id, int row, int column)
    {
        ID = id;
        RowIndex = row;
        ColumnIndex = column;
        if (movementScr)
        {
            movementScr.Initialise();
        }
    }

    public void Charge(int dangerNearby, bool danger, Action<CubeInfo> onChange)
    {
        
        _changeCallback = onChange;
        DangerNearby = dangerNearby;
        IsDangerous = danger;
        if(danger)
        {
            collider.material = bouncyMaterial;
        }
        isDanger = danger;
        danNearby = dangerNearby;
        ResetState();
    }

    public void Reveal()
    {
        rend.material = clickedMat;
        
        MoveCube();
        if (_button != null)
        {
            _button.interactable = false;
        }

        if (_textDisplay != null)
        {
            _textDisplay.enabled = true;
        }
    }

    public void StandDown()
    {
        if (_button != null)
        {
            _button.interactable = false;
        }

        if (Danger != null)
        {
            Danger.enabled = false;
        }

        if (_textDisplay != null)
        {
            _textDisplay.enabled = false;
        }
    }

    public void OnClick()
    {
        if(CameraManager.instance.isInGolfMode)
        {
            
            return;
        }
        PlayAudio();
        
        if (_button != null)
        {
            _button.interactable = false;
        }

        if(IsDangerous && Danger != null)
        {
            Danger.enabled = true;
           
           
        }
        else if(_textDisplay != null)
        {
            _textDisplay.enabled = true;
           
        }

        if(DangerFlag)
        {
            DangerFlag.gameObject.SetActive(false);
        }
        rend.material = clickedMat;
        GameManager.instance.IncrementClicks();
        _changeCallback?.Invoke(this);
    }

    public void PlayAudio()
    {

        if(_button.interactable == false)
        { return; }

        audSrc.pitch = UnityEngine.Random.Range(.25f, 1.75f);
        if (IsDangerous && Danger != null)
        {
            audSrc.PlayOneShot(explosion);
        }
        else
        {
            audSrc.PlayOneShot(blockHit);
        }
    }


    private void Awake()
    {
        _textDisplay = GetComponentInChildren<TMP_Text>(true);
        _button = GetComponentInChildren<Button>();
        _button.onClick.AddListener(OnClick);
        rend = GetComponent<MeshRenderer>();
        movementScr = GetComponent<CubeMovementController>();
        collider = GetComponent<BoxCollider>();
        audSrc = GetComponent<AudioSource>();
        //bouncyMaterial = colllider.material;
        ResetState();
    }

    private void ResetState()
    {
        movementScr.ResetPositions();
        SetDangerFlagDisabled();
        golfClicked = false;
        if (Danger != null)
        {
            Danger.enabled = false;
        }

        if (_textDisplay != null)
        {
            if (DangerNearby > 0)
            {
                _textDisplay.text = DangerNearby.ToString("D");
                _textDisplay.color = DangerColors[DangerNearby-1];
            }
            else
            {
                _textDisplay.text = string.Empty;
            }

            _textDisplay.enabled = false;
        }

        if (_button != null)
        {
            _button.interactable = true;
        }
        //colllider.material = bouncyMaterial;
    }

    public void HoveredEvent()
    {
        if (GameManager.instance.player.currentCube == null)
        {
            GameManager.instance.player.currentCube = this;
        }
    }
    public void HoverOverEvent()
    {
        if (!GameManager.instance.player.inspectMode)
        {
            GameManager.instance.player.currentCube = null;
        }
    }
    public void ShowNeighbours()
    {
        if(neighbourBoxes.Count > 0)
        {
            
            foreach(CubeInfo cube in neighbourBoxes)
            {
                cube.SwapOutMaterials(true);
            }
        }
    }


    public void SwapOutMaterials(bool shouldHighlight)
    {
        if(shouldHighlight)
        {
            rend.material = highlightedMat;
        }
        else if(golfClicked)
        {
            rend.material = mineMat;
        }
        else if(!IsActive)
        {
            rend.material = clickedMat;
           
        }
        else
        {
            rend.material = defMat;
        }
    }

    public void ToggleDangerFlag()
    {
        if(!DangerFlag)
        {
            return;
        }
        DangerFlag.gameObject.SetActive(!DangerFlag.gameObject.activeSelf);
    }
    public void SetDangerFlagDisabled()
    {
        if (!DangerFlag)
        {
            return;
        }
        DangerFlag.gameObject.SetActive(false);
    }

    public void MoveCube()
    {
        movementScr.MoveCubes();
        if(IsDangerous)
        {
            IsDangerous = false;
            rend.material = mineMat;
            _button.interactable = false;
        }
    }
}
