using UnityEngine;
using TMPro;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
public class CubeBasedUI : MonoBehaviour
{

    [SerializeField] private GameObject /*menu,*/ game, stats, result, golfBased;
    [SerializeField] private TMP_Text TimerText;
    [SerializeField] private TMP_Text ClickText;
    [SerializeField] private TMP_Text ResultText;
    [SerializeField] private TMP_Text ResultTimeText;
    [SerializeField] private TMP_Text ResultClickText;
    [SerializeField] private TMP_Text ResultShotText;
    [SerializeField] private TMP_Text specialResultText;

    private static readonly string[] ResultTexts = { "Game Over!", "You Win!!" };
    private static readonly float AnimationTime = 0.5f;

    [SerializeField] private List<string> specialMessages = new List<string>();

    AudioSource audSrc;
    [SerializeField] AudioClip normalWin, noMines, holeInOne, failed;


    private void OnEnable()
    {
        GameManager.instance.GamePaused += ToggleUI;
    }

    private void OnDisable()
    {
        GameManager.instance.GamePaused -= ToggleUI;
    }

    public void ToggleUI(bool show)
    {
       
        golfBased.SetActive(!show);
    }


    public void ShowGame()
    {
        CameraManager.instance.startInterp = true;
      
        game.SetActive(true);
        
        stats.SetActive(true);
    }

    public void ShowResult(bool success)
    {
        GameManager.instance.gameFinished = true;
        if (CameraManager.instance.isInGolfMode)
        {
            CameraManager.instance.golfInterp = true;
           
        }

        if(!success)
        {
            audSrc.PlayOneShot(failed);
        }
        else
        {
            if(GolfModeController.instance.shots <= 1)
            {
                audSrc.PlayOneShot(holeInOne);
            }
            else if(GolfModeController.instance.minesHit <= 0)
            {
                audSrc.PlayOneShot(noMines);
            }
            else
            {
                audSrc.PlayOneShot(normalWin);
            }
        }

        
        if (ResultText != null)
        {
            ResultText.text = ResultTexts[success ? 1 : 0];
            ResultTimeText.enabled = success;
            ResultClickText.enabled = success;
            ResultTimeText.text = TimerText.text;
            ResultClickText.text = ClickText.text;
            ResultShotText.text = GolfModeController.instance.shotText.text;
            specialResultText.gameObject.SetActive(success);
            if(GolfModeController.instance.minesHit <= 0)
            {
                specialResultText.text = specialMessages[Random.Range(0, specialMessages.Count)];
            }
            else
            {
                specialResultText.gameObject.SetActive(false);
            }
        }
        stats.SetActive(false);
        golfBased.SetActive(false);
        result.SetActive(true);
       
    }

    //public void HideMenu()
    //{
    //    menu.SetActive(false);
    //}

    public void HideGame()
    {
        game.SetActive(false);
        stats.SetActive(false);
    }

    public void HideResult()
    {
        result.SetActive(false);
    }

    public void UpdateTimer(double gameTime)
    {
        if (TimerText != null)
        {
            TimerText.text = FormatTime(gameTime);
        }
    }
    public void IncreaseClicks(int clicks)
    {
        if(ClickText != null)
        {
            ClickText.text = ($"Clicks: {clicks}");
        }
    }
    private void Awake()
    {
        stats.SetActive(false);
        result.SetActive(false);
        audSrc = GetComponent<AudioSource>();
        //game.SetActive(false);

        
    }

    private static string FormatTime(double seconds)
    {
        float m = Mathf.Floor((int)seconds / 60);
        float s = (float)seconds - (m * 60);
        string mStr = m.ToString("00");
        string sStr = s.ToString("00.000");
        return string.Format("{0}:{1}", mStr, sStr);
    }

}
