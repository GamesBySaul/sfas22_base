using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;



[CustomEditor(typeof(BoardGenerator))]
public class BoardEditorGenerator : Editor
{
    SerializedProperty cubeBoard;

    private void OnEnable()
    {
        cubeBoard = serializedObject.FindProperty("boardController");
    }


    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        serializedObject.Update();
     
        serializedObject.ApplyModifiedProperties();

        BoardGenerator generationScript = (BoardGenerator)target;

        if(GUILayout.Button("Generate Board"))
        {
            generationScript.DebugGenerate();
        }

        if (GUILayout.Button("Show Board"))
        {
            generationScript.ShowBoard();
        }

        if (GUILayout.Button("DeGenerate Board"))
        {
            generationScript.DestroyBoard();
        }
    }
}
#endif