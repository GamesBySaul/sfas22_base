using UnityEngine;
using UnityEngine.SceneManagement;
public class CubeBasedGame : MonoBehaviour
{
    [SerializeField] private CubeBasedBoard _board;
    [SerializeField] private CubeBasedUI _ui;
    private double _gameStartTime;
    private bool _gameInProgress;

    [SerializeField] GameObject golfObjectsHolder;

    bool trackTime = true;
    private void OnEnable()
    {
        GameManager.instance.GamePaused += TrackingTime;
       

    }
    private void OnDisable()
    {
        GameManager.instance.GamePaused -= TrackingTime;

    }

    void TrackingTime(bool shouldTrack)
    {
        trackTime = !shouldTrack;
    }

    public void OnClickedNewGame()
    {

        CameraManager.instance.ResetPos();
        if (golfObjectsHolder != null)
        {
            golfObjectsHolder.SetActive(true);
        }
        
        if (_board != null)
        {
            if(!_board.boardIsGenerated)
            {
                _board.BoardGeneration();
                _board.CheckNeighbourBoxes();
            }
            _board.Setup(BoardEvent);
            _board.RechargeBoxes();
            _board.ToggleCubes(true);

        }

        if (_ui != null)
        {
            //_ui.HideMenu();
            _ui.ShowGame();
        }
    }

    public void OnClickedExit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }

    public void OnClickedReset()
    {
        if (_board != null)
        {
            _board.Clear();
            _board.ToggleCubes(false);
            _board.BoardDeGeneration();
          
        }

        if (_ui != null)
        {
            _ui.HideResult();
            //_ui.ShowMenu();
            GameManager.instance.ResetClicks();
        }

        if (golfObjectsHolder != null)
        {
            golfObjectsHolder.SetActive(false);
        }
        
        GolfModeController.instance.DeleteGolfObjects();

        MainMenuController.instance.SetSeedText(ListHelper.seedToUse);
       
        MainMenuController.instance.GoToGame(-1);
        
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
       
    }

    private void Awake()
    {
        //_board = FindObjectOfType<ThreeDBoard>();
        
        _gameInProgress = false;
      
    }

    private void Start()
    {
        if (_board != null)
        {
            _board.Setup(BoardEvent);
        }

        //if (_ui != null)
        //{
        //    _ui.ShowMenu();
        //}
        OnClickedNewGame();
    }

    private void Update()
    {

        if (_ui != null && trackTime)
        {
            _ui.UpdateTimer(_gameInProgress ? Time.timeAsDouble - _gameStartTime : 0.0);
        }
    }

    private void BoardEvent(CubeBasedBoard.Event eventType)
    {
        if(eventType == CubeBasedBoard.Event.ClickedDanger && _ui != null)
        {
            //_ui.HideGame();
            Camera.main.transform.parent = null; //Risk Checker
            _ui.ShowResult(success: false);
   
        }

        if (eventType == CubeBasedBoard.Event.Win && _ui != null)
        {

            //Debug.Log("All mines cleared!");
            //Debug.Log("And shot done?");

             Camera.main.transform.parent = null; //Risk Checker

              _ui.ShowResult(success: true);
        }

        if (!_gameInProgress)
        {
            _gameInProgress = true;
            _gameStartTime = Time.timeAsDouble;
        }
    }
}
