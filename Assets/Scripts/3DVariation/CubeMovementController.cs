using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovementController : MonoBehaviour
{

    Vector3 initialPosition;
    Vector3 targetPosition;

    bool doMove;
    bool atPosition;

    float t;
   public void Initialise()
    {
        initialPosition = this.gameObject.transform.position;
        targetPosition = new Vector3(initialPosition.x, initialPosition.y, 1);
    }

    public void MoveCubes()
    {
      
        doMove = true;
    }

    private void Update()
    {
       if(doMove)
        {
            t += Time.deltaTime;
            this.gameObject.transform.position = Vector3.Lerp(initialPosition, targetPosition, t);
            if(Mathf.Approximately(this.gameObject.transform.position.z, targetPosition.z))
            {
                doMove = false;
            }
        }
    }

    public void ResetPositions()
    {
        this.gameObject.transform.position = initialPosition;
    }



}
