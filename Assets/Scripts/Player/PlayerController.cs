using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public CubeInfo currentCube;

    public bool inspectMode;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(2) && !CameraManager.instance.isInGolfMode)
        {
            inspectMode = true;
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
           
            GameManager.instance.BoardController.CheckIfBoxHit(mousePos);
            if (currentCube != null)
            {
                currentCube.ShowNeighbours();
            }
        }

        if(Input.GetMouseButtonUp(2) && !CameraManager.instance.isInGolfMode)
        {
            inspectMode = false;
            if (currentCube != null)
            {
                
                foreach (CubeInfo cube in currentCube.neighbourBoxes)
                {
                    cube.SwapOutMaterials(false);
                }
            }
            currentCube = null;
        }

        if(Input.GetMouseButtonDown(1) && !CameraManager.instance.isInGolfMode)
        {
            if(currentCube == null)
            {
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                GameManager.instance.BoardController.CheckIfBoxHit(mousePos);
            }
            
            if(currentCube != null && currentCube.IsActive)
            {
                currentCube.ToggleDangerFlag();
            }
            
        }
        
    }
}
