using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{

    public static CameraManager instance;

    void Awake()
    {
        instance = this;
    }

    [SerializeField] Camera minesweeperCam;

    [SerializeField] const float sizeMultiplier = 0.6f; //worked out .6 was the best size as the camera looks the best on a 5x5 when at size 3
    [SerializeField] float endYPosition = 0.5f; //camera starts off at .5f - if the board size is even then go to 1, if not stay at .5f

    float startingSize;
    float endSize;

    float startingYPos;

    public bool startInterp;
    public bool golfInterp;

    public bool isInGolfMode = false;

    float t = 0;
    public float interpSpeed;
    Vector3 minePos;
   Quaternion mineRot;
    public Vector3 golfPos;
    Vector3 golfInitialPos;
    [SerializeField] Quaternion golfRot;

    public GameObject panObject;
    public GameObject ballParent;
    [SerializeField] float panSpeed;
    public bool canRotateAround;
    public bool fixCamRotation;

    float dirMultiplier = 0;

    public GolfBallController ballScr;
    //adding a ? - makes it a nullable value type - means you can check for null
    [SerializeField] Quaternion? childRot;
    public Quaternion visibleChildRot;
    // Start is called before the first frame update
    void Start()
    {
        startingSize = minesweeperCam.orthographicSize;
        startingYPos = minesweeperCam.transform.position.y;

        mineRot = minesweeperCam.transform.rotation;
        golfInitialPos = golfPos;
    }

    public void SetEndSize(float boardWidth)
    {
       

        endSize = sizeMultiplier * boardWidth;

        endYPosition = (boardWidth % 2 == 0) ? 1f : 0.5f; 
    }

    // Update is called once per frame
    void Update()
    {
        if (childRot.HasValue)
        {
            visibleChildRot = childRot.Value;
        }
        if(startInterp)
        {
            SizeAndPosition();
        }
        if(golfInterp)
        {
            ToggleViewMode();
        }
        if(fixCamRotation)
        {
            FixRotation();
        }
        if(panObject && isInGolfMode)
        {
            if (canRotateAround && Input.GetAxis("Horizontal") != 0f)
            {
                
                panObject.transform.Rotate(Vector3.forward * panSpeed * Time.deltaTime * dirMultiplier, Space.World);
               
                
            }
        }
        if(Input.GetAxis("Horizontal") <= 0)
        {
            dirMultiplier = -1f;
        }
        else
        {
            dirMultiplier = 1f;
        }
    }
    public void ResetPos()
    {
        golfPos = golfInitialPos;
    }
    public void ToggleViewMode()
    {
        t += Time.deltaTime;
        minesweeperCam.orthographic = isInGolfMode;
        minesweeperCam.transform.parent = null;
        if (!isInGolfMode)
        {
            minesweeperCam.transform.SetParent(ballParent.transform);
            minesweeperCam.transform.localRotation = Quaternion.Lerp(mineRot, golfRot, t * interpSpeed);

            minesweeperCam.transform.localPosition = Vector3.Lerp(minesweeperCam.transform.localPosition, ballScr.offset, t * interpSpeed);
          

                if (Quaternion.Angle(minesweeperCam.transform.localRotation, golfRot) < .1f && Vector3.Distance(minesweeperCam.transform.localPosition, ballScr.offset) < .4f)
                {       
                if (!childRot.HasValue)
                {

                    childRot = minesweeperCam.transform.localRotation;
                }
                //minesweeperCam.transform.localRotation = Quaternion.Lerp(minesweeperCam.transform.localRotation, childRot.Value, t * interpSpeed); //Lerp this
                ////Debug.LogError("TEST");
                //if (Quaternion.Angle(minesweeperCam.transform.localRotation, childRot.Value) < .1f)
                //{
                //}
                isInGolfMode = true;
                golfInterp = false;
                t = 0;
                }
            
        }
        else
        {
            
            
            minesweeperCam.transform.position = Vector3.Lerp(golfPos + ballScr.offset, minePos, t * interpSpeed);
            minesweeperCam.transform.rotation = Quaternion.Lerp(golfRot, mineRot, t * interpSpeed);
            if (minesweeperCam.transform.position == minePos && CompareRotations(mineRot))
            {
                isInGolfMode = false;
                golfInterp = false;
                t = 0;
            }
        }
    }

    private void SizeAndPosition()
    {
        t += Time.deltaTime;
        minesweeperCam.orthographicSize = Mathf.SmoothStep(startingSize, endSize, t);



        minesweeperCam.transform.position = new Vector3(minesweeperCam.transform.position.x, Mathf.SmoothStep(startingYPos, endYPosition, t), minesweeperCam.transform.position.z);

        if (Mathf.Approximately(minesweeperCam.orthographicSize, endSize) && Mathf.Approximately(minesweeperCam.transform.position.y, endYPosition))
        {
            startInterp = false;
            t = 0;
            minePos = minesweeperCam.transform.position;
            GolfModeController.instance.ToggleUI();
        }
    }

    bool CompareRotations(Quaternion compareRot)
    {
        return 1 - Mathf.Abs(Quaternion.Dot(minesweeperCam.transform.rotation, compareRot)) < .2f;
    }

    void FixRotation()
    {
        minesweeperCam.transform.rotation = Quaternion.Lerp(minesweeperCam.transform.rotation, golfRot, t * interpSpeed);
        if(CompareRotations(golfRot))
        {
            fixCamRotation = false;
        }
    }

}
