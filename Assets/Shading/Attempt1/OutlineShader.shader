Shader "Unlit/OutlineShader"
{
    Properties
    {
        _Thickness("Thickness", Float) = 1 //Amount to extrude outline mesh
        _Color("Colour", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "RenderPipeline" = "UniversalPipeline"}
        LOD 100

        Pass
        {
        Name "Outlines"
          //Cull front faces
        Cull Front

        HLSLPROGRAM
        //URP Requirements
        #pragma prefer_hlslcc gles
        #pragma exclude_renderers d3d11_9x

        //Register our functions
        #pragma vertex Vertex
        #pragma fragment Fragment

        //Include hlsl file
        #include "OutlineShader.hlsl"

        ENDHLSL
        }
    }
}
